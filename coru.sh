#!/bin/bash
#
# compile&run
#
# Copyright (c) 2022, Hagen Schnuch
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
#
###

# --- Settings ---
for arg in $@; do
	case "$arg" in
		"noc" | "noconfig" ) NO_CONFIG=1 ;;
		"preb" | "prebuild" | "prep" | "prepare" ) PREB=1 ;;
	esac
done
CORU_CONF=${CORU_CONF:-"coru.conf"}
test -z "$NO_CONFIG" -a -s "$CORU_CONF" && source $CORU_CONF

CC=${CC:-"clang"}
LD=${LD:-"$CC"}

DBG=${DBG:-"lldb"}

DEB_FLAGS=${DEB_FLAGS:-"-g -DDEBUG"}
REL_FLAGS=${REL_FLAGS:-"-O3"}
SAN_FLAGS=${SAN_FLAGS:-"-fsanitize=address -fsanitize=undefined -fsanitize=leak"}

CFLAGS_DEFAULT=${CFLAGS_DEFAULT:-"-Wall -Wformat -Wextra -fno-exceptions"}
test $CC = "clang" && CFLAGS_DEFAULT="$CFLAGS_DEFAULT -fcolor-diagnostics"
#CFLAGS_DEFAULT=${CFLAGS:-"/c /nologo /Zi /MD /W4"}

#CFLAGS=${CFLAGS:-""}
CFLAGS="$CFLAGS_DEFAULT $CFLAGS"

#LFLAGS=${LFLAGS:-"/debug:full"}

SRC_DIR=${SRC_DIR:-"src"}
OBJ_DIR=${OBJ_DIR:-"obj"}
OUT_DIR=${OUT_DIR:-"bin"}

#SRC=${SRC:-"$SRC_DIR/**/*.c*"}
SRC=${SRC:-"$SRC_DIR/main.c"}

PREB_FILE=${PREB_FILE:-"prebuild.hpp"}
PREB=${PREB:-0}
case "$PREB" in "1" | "on" | "true" ) DO_PREBUILD=1 ;; esac

THREADS=${THREADS:-$(getconf _NPROCESSORS_ONLN)}
THREADS=${THREADS:-4}

#QUIET=${QUIET:-0}

# --- Functions ---
shopt -s globstar
shopt -s extglob

log() {
	text="$1"
	shift

	case "$QUIET" in
		"1" | "on" | "true" ) ;;
		* ) printf "$text" "$@" ;;
	esac
}

printResult() { # $1 = res, $2 = output
	if test $1 -eq 0; then
		log "\e[32mDone\e[0m\n"
	else
		log "\e[31mFail\e[0m\n"
	fi
	test "$2" && printf "$2\n"
}

prebuild() {
	prebSrc="$SRC_DIR/$PREB_FILE"
	if test ! -s $prebSrc; then
		printf "File not found: $prebSrc\n"
		return 1
	fi
	prebObj="$OBJ_DIR/$PREB_FILE.gch"
	if test -s $prebObj; then
		mTimeSrc=$(stat -c %Y $prebSrc)
		mTimeOut=$(stat -c %Y $prebObj)
		test $mTimeSrc -lt $mTimeOut && return 0
	fi
	log "Building %-35s " $prebSrc
	test -d $OBJ_DIR || mkdir -p $OBJ_DIR
	output=$($CC -c $CFLAGS $INCL $prebSrc -o $prebObj 2>&1)
	res=$?
	printResult $res "$output"
	return $res
}

compile() {
	src="$1"
	if test ! -s "$src"; then
		printf "File not found: $src\n"
		return 1
	fi

	if test -n "$DO_PREBUILD"; then
		prebFlag=${PREB_FLAG:-"-include $OBJ_DIR/$PREB_FILE"}
	else
		unset prebFlag
	fi

	filename=${src##*/}
	if test $SRC_NUM -gt 1; then
		objFile="$OBJ_DIR/${filename%.c*}.o"

		if test -s $objFile; then
			mtimeSrc=$(stat -c %Y $src)
			mtimeObj=$(stat -c %Y $objFile)
			test $mtimeObj -ge $mtimeSrc && return 2
		fi

		output=$($CC -c $CFLAGS $prebFlag $INCL -o $objFile $src 2>&1)
	else
		NAME=${NAME:-"${filename%.c*}"}
		outPath="$OUT_DIR/$NAME$EXT"
		test -d $OUT_DIR || mkdir -p $OUT_DIR
		output=$($CC $CFLAGS $prebFlag $INCL -o $outPath $src $LFLAGS $LIBP $LIBS 2>&1)
	fi
	res=$?
	log "Building %-35s " $src
	printResult $res "$output"
	return $res
}

link() {
	NAME=${NAME:-"main"}
	outPath="$OUT_DIR/$NAME$EXT"
	if test -s $outPath; then
		mtimeOut=$(stat -c %Y $outPath)
		mtimeObj=0
		for f in $OBJ_DIR/*; do
			mtimeTmp=$(stat -c %Y $f)
			test $mtimeTmp -gt $mtimeObj && mtimeObj=$mtimeTmp
		done
		test $mtimeOut -gt $mtimeObj && return 2
	fi

	log " Linking %-35s " $outPath
	test -d $OUT_DIR || mkdir -p $OUT_DIR
	output=$($LD -o $outPath $OBJ_DIR/*.o $LFLAGS $LIBP $LIBS 2>&1)
	res=$?
	printResult $res "$output"
	return $res
}

build() {
	SRC_ARR=($SRC)
	SRC_NUM=${#SRC_ARR[@]}

	if test -n "$DO_PREBUILD"; then
		prebuild
		res=$?
		test $res -ne 0 && return $res
	fi

	test $SRC_NUM -gt 1 -a ! -d $OBJ_DIR && mkdir -p $OBJ_DIR
	fail=0
	tCount=0
	for f in ${SRC_ARR[@]}; do
		if test $tCount -ge $THREADS; then
			wait -n
			res=$?
			test $res -eq 1 && fail=1
			((tCount--))
		fi
		compile $f &
		((tCount++))
	done
	while test $tCount -gt 0; do
		wait -n
		res=$?
		test $res -eq 1 && fail=1
		((tCount--))
	done

	if test $SRC_NUM -gt 1 -a $fail -eq 0; then
		link
		res=$?
		if test $res -eq 2; then
		   	log " Nothing to do\n"
			res=0
		fi
	fi
	return $fail
}

clean() {
	if test -d $OBJ_DIR; then
		rm $OBJ_DIR/*.o   2> /dev/null
		rm $OBJ_DIR/*.obj 2> /dev/null
		rm $OBJ_DIR/*.gch 2> /dev/null
		rm $OBJ_DIR/*.pch 2> /dev/null
		test -z "$(ls -A $OBJ_DIR)" && rmdir $OBJ_DIR
	fi
	# if test -d $OUT_DIR; then
	# 	outPath="$OUT_DIR/$NAME$EXT"
	# 	rm "$outPath" 2> /dev/null
	# 	test -z "$(ls -A $OUT_DIR)" && rmdir $OUT_DIR
	# fi
}

run() {
	NAME=${NAME:-"main"}
	outPath="$OUT_DIR/$NAME$EXT"
	if test ! -s $outPath; then
		printf "File not found: $outPath\n"
		return 1
	fi
	log " Running $outPath\n"
	cd $OUT_DIR
	./$NAME$EXT
	cd - > /dev/null
}

debug() {
	cd $OUT_DIR
	$DBG ./$NAME$EXT
	cd - > /dev/null
}


# --- Input handling ---
for arg in $@; do
	case "$arg" in
		"bu" | "build" | "co" | "com" | "compile" ) DO_BUILD=1 ;;
		"cl" | "clean" | "clear" ) DO_CLEAN=1 ;;
		"quiet" ) QUIET=1 ;;
		"re" | "reb" | "rebuild" ) DO_CLEAN=1; DO_BUILD=1 ;;
		"rel" | "release" ) DO_RELEASE=1 ;;
		"san" | "sanitize" ) DO_SANITIZE=1 ;;
		"run" ) DO_RUN=1 ;;
		"db" | "dbg" | "deb" | "debug" ) DO_DEBUG=1 ;;
		*.c* ) SRC="$arg"; SINGLE="c" ;;
		*.h* ) SRC="$arg"; SINGLE="h" ;;
	esac
done

test -z "$DO_RELEASE" -a -z "$DO_SANITIZE" && \
	CFLAGS="$CFLAGS $DEB_FLAGS"
test -n "$DO_RELEASE" && CFLAGS="$CFLAGS $REL_FLAGS"
if test -n "$DO_SANITIZE"; then
	CFLAGS="$CFLAGS $SAN_FLAGS"
	LFLAGS="$LFLAGS $SAN_FLAGS"
	test "$CC" = "gcc" && CC="g++"
	test "$CC" = "clang" && CC="clang++"
	test "$LD" = "gcc" && LD="g++"
	test "$LD" = "clang" && LD="clang++"
fi

if test "$SINGLE"; then
	SRC_DIR=$(dirname $SRC)
	OUT_DIR="."
	OBJ_DIR="."
	QUIET=${QUIET:-1};
	if test "$SINGLE" = "c"; then
		NAME="${SRC%.c*}"
	elif test "$SINGLE" = "h"; then
		NAME="$SRC.gch";
		DO_BUILD=1
		unset DO_RUN
	fi
fi

if test -z "$DO_BUILD" -a -z "$DO_RUN" -a -z "$DO_CLEAN" -a -z "$DO_DEBUG"; then DO_BUILD=1; DO_RUN=1; fi

res=0
if test -n "$DO_CLEAN"; then clean; res=$?; fi
if test $res -eq 0 -a -n "$DO_BUILD"; then build; res=$?; fi
if test $res -eq 0 -a -n "$DO_RUN"; then run; res=$?; fi
if test $res -eq 0 -a -n "$DO_DEBUG"; then build && debug; res=$?; fi

exit $res
