# coru

Coru (compile & run) is a simple build tool for C/C++ written in Bash.

The motivation behind this is that I was bothered with some of make's
limitations and that I wanted the absolute minimum of a config file to get
started with a new project.

It supports wildcards for source files and parallel builds.
It's nowhere near complete or bulletproof at the moment, but it already
helped me a lot.

![Example of coru's output #1](screenshots/exampleOutput1.png "Example of coru's output #1")
![Example of coru's output #2](screenshots/exampleOutput2.png "Example of coru's output #2")

## Usage

Simply call `coru` to potentially prebuild a specified header, build all
(changed) source files in the "src" directory and run the executable on a
successful build.

Create a "coru.conf" file in the working directory to specify the source or
build directory, as well as compiler and linker flags or other options. (See
"[coru.conf.sample](coru.conf.sample)")

Use environment variables if you don't want a config file or need to overwrite
it temporarily, e.g.: `CFLAGS="-O3 -Wextra" coru`
<br><br>

Argument | Description
-------- | -----------
`bu[ild]`, `co[mpile]` | only compile and link but do not run
`cl[ean]`    | clean up object files and prebuild header
`noc[onfig]` | ignore config file
`preb[uild]` | use prebuild header file
`quiet`      | do not print "Building/Linking/Running xxx"
`re[build]`  | same as using `clean` and `build`
`rel[ease]`  | use compiler flags for an optimized release build
`run`        | run without building
`deb[ug]`    | run inside debugger
`san[itize]` | use compiler flags for a slow analyzing build
`example.c`  | only build (and afterwards run) example.c to an executable named "example" (instead of "a.out")
`example.h`  | only build a header file

Multiple arguments may be used together, e.g.: `coru re run san`.

## License

[MPL-2.0 (Mozilla Public License Version 2.0)](https://www.mozilla.org/en-US/MPL/2.0/)
